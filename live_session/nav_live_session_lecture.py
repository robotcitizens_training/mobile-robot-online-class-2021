#!/usr/bin/env python3
import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
from tf.transformations import quaternion_from_euler
import tf
from geometry_msgs.msg import Pose, Point, Quaternion

class RobotNavigation():
    def __init__(self):
        rospy.init_node('nav_test', anonymous=False)
        rospy.on_shutdown(self.shutdown)
        #tell the action client that we want to spin a thread by default
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        rospy.loginfo("wait for the action server to come up")
        #allow up to 5 seconds for the action server to come up
        if self.move_base.wait_for_server(rospy.Duration(5)):
            rospy.loginfo("move_base action server is avaliable")
            rospy.loginfo("Ready to go !!")

        # create dictionary for store the location
        self.location_dict = {'home_position': [0.48796647461338166, 0.14300677582394788, -0.1605646820405635]}

        # rospy.spin()
    
    def go_to_location(self, location_name):
        result = False
        try:
            x,y,theta = self.location_dict[location_name]       
            x,y,theta = float(x), float(y), float(theta)
            #convert euler to quanternion
            q = quaternion_from_euler(0,0,theta) 
            goal = MoveBaseGoal()        
            goal.target_pose.header.frame_id = 'map'        
            goal.target_pose.header.stamp = rospy.Time.now()        
            goal.target_pose.pose = Pose(Point(x, y, 0.000), Quaternion(q[0], q[1], q[2], q[3]))            
            self.move_base.send_goal(goal)        
            success = self.move_base.wait_for_result(rospy.Duration(60))
            state = self.move_base.get_state()        
            if success and state == GoalStatus.SUCCEEDED:            
                # We made it!            
                result = True        
            else:            
                self.move_base.cancel_goal()        
                self.goal_sent = False
        except:
            pass        
        
        return result
        
    def shutdown(self):
        stop_goal = MoveBaseGoal()
        self.move_base.send_goal(stop_goal)
        rospy.loginfo("Stop")

    def get_location(self):
        self.listener = tf.TransformListener() 
        rate = rospy.Rate(10.0)
        get_location = False
        while not rospy.is_shutdown() and not get_location:
            try:
                (trans,rot) = self.listener.lookupTransform("/map","/base_link", rospy.Time(0))
                if trans != None:
                    get_location = True

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
        rate.sleep()
        euler = tf.transformations.euler_from_quaternion(rot)
        return trans[0], trans[1], euler[2]

if __name__ == '__main__':
    try:
        robot_nav = RobotNavigation()
        print(robot_nav.get_location())
        robot_nav.go_to_location('home_position')
    except rospy.ROSInterruptException:
        rospy.loginfo("Exception thrown")
